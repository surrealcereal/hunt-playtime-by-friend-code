#include "api.hpp"

#include <curl/curl.h>

#include <iostream>
#include <map>
#include <string>

#include "user.hpp"
#include "errorstatus.hpp"
#include "json.hpp"

using json = nlohmann::json;
using string = std::string;

size_t _writeFunction(void* ptr, size_t size, size_t nmemb, string* data) {
    data->append((char*)ptr, size * nmemb);
    return size * nmemb;
}

string getWebpageContent(const char* url) {
    auto curl{curl_easy_init()};
    curl_easy_setopt(curl, CURLOPT_URL, url);
    string response_string{};
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, _writeFunction);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response_string);
    curl_easy_perform(curl);
    curl_easy_cleanup(curl);
    curl = NULL;
    return response_string;
}

string steamAPICall(const string& callType, User& user, const string& steamAPIKey) {
    static std::map<string, std::map<string, string>> apiCallMap{
        {"playtime", {{"apiCall", "IPlayerService/GetOwnedGames/v0001"}, {"steamIDQueryString", "&steamid="}}},
        {"username", {{"apiCall", "ISteamUser/GetPlayerSummaries/v0002"}, {"steamIDQueryString", "&steamids="}}}};
    const string url{{"https://api.steampowered.com/" + apiCallMap[callType]["apiCall"] + "/?key=" + steamAPIKey +
                      apiCallMap[callType]["steamIDQueryString"] + std::to_string(user.getSteamID64())}};
    const char* urlc{url.c_str()};  // convert to C type string as curl does not accept std::string
    return getWebpageContent(urlc);
}

json getSteamJSON(const string& callType, User& user, const string& steamAPIKey, ErrorStatus& errorStatus) {
    const string apiResponse{steamAPICall(callType, user, steamAPIKey)};
    try {
        return json::parse(apiResponse);
    } catch (const json::parse_error& ex) {
        std::cout << "Wrong friend code given.";
        errorStatus.set();
        return json();
    }
}
