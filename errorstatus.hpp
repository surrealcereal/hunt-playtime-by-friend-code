#ifndef ERRORSTATUS_HPP
#define ERRORSTATUS_HPP

class ErrorStatus {
    bool error{false};

   public:
    void set() { error = true; };
    void unset() { error = false; };
    explicit operator bool() const { return error; }
};

#endif
