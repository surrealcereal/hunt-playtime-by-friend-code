#include "console.hpp"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include "playtimeErrorCodes.hpp"
#include "user.hpp"

using string = std::string;

namespace Colors {
const string magenta{"\033[95m"};
const string blue{"\033[94m"};
const string cyan{"\033[96m"};
const string green{"\033[92m"};
const string yellow{"\033[93m"};
const string red{"\033[91m"};

const string end{"\033[0m"};
}  // namespace Colors

string color(const string& text, const string& color) { return color + text + Colors::end; }

void produceBlacklistWarning(const std::vector<int>& blacklist, User& user) {
    if (std::find(blacklist.begin(), blacklist.end(), user.getSteamID32()) !=
        blacklist.end()) {  // if steamid32 in blacklist
        std::cout << color("Warning, player " + user.getUsername() + " is in blacklist!", Colors::red) << '\n';
    }
}

double round(double value, int significantDigits) {
    const double multiplier{std::pow(10.0, significantDigits)};
    return std::ceil(value * multiplier) / multiplier;
}

void produceHoursMessage(User& user) {
    string coloredUsername{color(user.getUsername(), Colors::blue)};
    if (user.getPlaytime() > 0) {
        std::cout << "Player " << coloredUsername << " has "
                  << color(std::to_string(round(user.getPlaytime(), 1)), Colors::magenta) << " hours in Hunt: Showdown."
                  << '\n';
        return;
    }
    switch (static_cast<int>(user.getPlaytime())) {
        case playtimeErrorCodes::HAS_NOT_PLAYED_HUNT:
            std::cout << "Player " << coloredUsername
                      << color(" owns Hunt: Showndown but has never played it.", Colors::green) << '\n';
            return;
        case playtimeErrorCodes::GAMES_TAB_PRIVATE:
            std::cout << "Player " << coloredUsername << " has " << color("game information private.", Colors::yellow)
                      << '\n';
            return;
        case playtimeErrorCodes::PROFILE_PRIVATE:
            std::cout << "Player " << coloredUsername << " has " << color("a private profile.", Colors::yellow) << '\n';
            return;
        case playtimeErrorCodes::NO_HUNT_IN_LIBRARY:
            std::cout << "Player " << coloredUsername
                      << color(" does not have Hunt: Showdown in their library.", Colors::cyan) << '\n';
            return;
        case playtimeErrorCodes::PLAYTIME_INFO_PRIVATE:
            std::cout << "Player " << coloredUsername << " has "
                      << color("playtime information private.", Colors::yellow) << '\n';
            return;
        default:
            std::cout << color("Error! Cannot retrieve information regarding player ", Colors::red) << coloredUsername
                      << ".\n";
            return;
    }
}
