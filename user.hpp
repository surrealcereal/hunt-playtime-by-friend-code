#ifndef USER_HPP
#define USER_HPP

#include <string>

#include "errorstatus.hpp"

class User {
    std::string username{};
    bool profilePrivate{};
    bool playtimeInfoPrivate{true};
    long long steamid64{};
    int steamid32{};
    double playtime{};
    void setSteamID64();
    void setUserDetails(const std::string&, ErrorStatus&);
    void setHuntPlayTime(const std::string&, ErrorStatus&);

   public:
    User(int, const std::string&, ErrorStatus&);
    std::string getUsername();
    int getSteamID32();
    long long getSteamID64();
    double getPlaytime();
};
#endif
