#include "parameters.hpp"

#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

#include "api.hpp"

using string = std::string;

void Parameters::populateBlacklist() {
    std::ifstream blacklistFile(blacklistFileLocation);
    if (!blacklistFile) {
        if (blacklistFileLocation == "blacklist.txt") {
            std::cout << "No blacklist file found in current directory and none specified via -b or --blacklist flag";
        } else {
            std::cout << "No blacklist file exists at " << blacklistFileLocation;
        }
        std::cout << ", blacklist checks disabled.\n"
                     "This should be a file of friend codes, one on each line.\n"
                     "Either create the specified \"blacklist.txt\" in the current working directory or point to a "
                     "blacklist file via -b or --blacklist.\n";
        blacklistExists = false;
        return;
    }
    std::istream_iterator<int> start(blacklistFile), end;
    blacklist = std::vector<int>(start, end);
}

void Parameters::populateSteamAPIKeyFromFile() {
    std::ifstream steamAPIKeyFile(steamAPIKeyFileLocation);
    if (!steamAPIKeyFile) {
        if (steamAPIKeyFileLocation == "steamAPIKey.txt") {
            std::cout << "No SteamAPI key file found in current directory and key not specified via -k or --key flag";
        } else {
            std::cout << "No SteamAPI key file exists at " << steamAPIKeyFileLocation;
        }
        std::cout << ", exiting...\n"
                     "Your SteamAPI key is obtainable here: https://steamcommunity.com/dev/registerkey\n"
                     "Either create a \"steamAPIKey.txt\" containg your SteamAPI key in the current working directory, "
                     "point to a "
                     "SteamAPI key file via --keyfile or specify the SteamAPI key directly via -k or --key.\n";
        exit(1);
    }
    string key{};
    getline(steamAPIKeyFile, key);
    steamAPIKey = key;
}

void printHelp() {
    std::cout << "Usage:\n"
                 "\thunttime [FLAGS] [OPTIONS]\n"
                 "FLAGS:\n"
                 "\t-h, --help\t\tDisplay this help information\n"
                 "\n"
                 "OPTIONS:\n"
                 "\t-k, --key <steam-api-key> \t\tSpecify SteamAPI key directly from the command line\n"
                 "\t-b, --blacklist <blacklist-file-location>\t\tSpecify blacklist file location\n"
                 "\t--keyfile <steam-api-key-file-location>\t\tSpecify SteamAPI key file location\n"
                 "\n"
                 "\t -q, --query <friend-code> \t\tSpecify query directly from the command line (exits after querying)\n";
}

void Parameters::argvParser(int argc, char* argv[]) {
    string arg{};
    for (int i = 1; i < argc; ++i) {  // skip argv[0], which is the program itself
        arg = argv[i];
        if (arg == "-k" || arg == "--key") {
            steamAPIKey = argv[i + 1];
            ++i;  // don't parse next argument as it's the value of this flag
        } else if (arg == "-b" || arg == "--blacklist") {
            blacklistFileLocation = argv[i + 1];
            ++i;
        } else if (arg == "--keyfile") {
            steamAPIKeyFileLocation = argv[i + 1];
            ++i;
        } else if (arg == "-q" || arg == "--query") {
            query = atoi(argv[i + 1]);
            ++i;
        } else if (arg == "-h" || arg == "--help") {
            printHelp();
            exit(0);
        } else {
            std::cout << "unknown option: " << arg << '\n';
            printHelp();
            exit(1);
        }
    }
}

bool isSteamAPIKeyValid(const string& steamAPIKey) {
    // using normally private func to make dummy test to account specified on
    // https://developer.valvesoftware.com/wiki/Steam_Web_API
    const string url{"http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=" + steamAPIKey +
                     "&steamid=76561197960434622"};
    string apiTestResponse{getWebpageContent(url.c_str())};
    return (apiTestResponse.find("Unauthorized") ==
            string::npos);  // the return is Unauthorized on this request but Forbidden on other types, be careful
}

Parameters::Parameters(int argc, char* argv[]) {
    argvParser(argc, argv);
    if (steamAPIKey == "") {
        populateSteamAPIKeyFromFile();
    }
    if (!isSteamAPIKeyValid(steamAPIKey)) {
        std::cout << "The specified SteamAPI key " << steamAPIKey << " is invalid, exiting...\n";
        exit(1);
    }
    populateBlacklist();
}

string Parameters::getSteamAPIKey() { return steamAPIKey; }
bool Parameters::doesBlacklistExist() { return blacklistExists; }
std::vector<int> Parameters::getBlacklist() { return blacklist; }
int Parameters::getQuery() { return query; }
