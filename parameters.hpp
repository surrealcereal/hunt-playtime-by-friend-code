#ifndef PARAMETERS_HPP
#define PARAMETERS_HPP

#include <string>
#include <vector>

class Parameters {
    std::string blacklistFileLocation{"blacklist.txt"};
    bool blacklistExists{true};
    std::string steamAPIKeyFileLocation{"steamAPIKey.txt"};
    std::string steamAPIKey{};
    std::vector<int> blacklist{};
    int query{};

    void populateBlacklist();
    void populateSteamAPIKeyFromFile();
    void argvParser(int, char**);

   public:
    Parameters(int, char**);
    std::string getSteamAPIKey();
    bool doesBlacklistExist();
    std::vector<int> getBlacklist();
    int getQuery();
};

#endif
