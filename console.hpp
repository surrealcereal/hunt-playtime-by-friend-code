#ifndef CONSOLE_HPP
#define CONSOLE_HPP

#include <vector>

#include "user.hpp"

void produceBlacklistWarning(const std::vector<int>& blacklist, User& user);
void produceHoursMessage(User& user);

#endif
