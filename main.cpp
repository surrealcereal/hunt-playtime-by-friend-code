#include <iostream>
#include <limits>

#include "console.hpp"
#include "errorstatus.hpp"
#include "parameters.hpp"
#include "user.hpp"

int getUserQuery() {
    int steamid32{};
    std::cout << "Please enter friend code of the player. ";
    std::cin >> steamid32;
    while (!std::cin) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Wrong friend code given.\n"
                     "Please enter a correct friend code. ";
        std::cin >> steamid32;
    }
    return steamid32;
}

int main(int argc, char* argv[]) {
    Parameters parameters(argc, argv);
    ErrorStatus errorStatus{};

    while (true) {
        int steamid32{};
        if (parameters.getQuery()) {
            steamid32 = parameters.getQuery();
        } else {
            steamid32 = getUserQuery();
        }
        User user(steamid32, parameters.getSteamAPIKey(), errorStatus);
        if (parameters.doesBlacklistExist()) {
            produceBlacklistWarning(parameters.getBlacklist(), user);
        }
        produceHoursMessage(user);
        if (errorStatus) {
            errorStatus.unset();
        }
        if (parameters.getQuery()) exit(0);  // quit after querying when running with -q or --query
    }
    return 0;
}
