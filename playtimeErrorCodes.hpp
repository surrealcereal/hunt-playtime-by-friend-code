#ifndef PLAYTIMEERRORCODES_HPP
#define PLAYTIMEERRORCODES_HPP
namespace playtimeErrorCodes {
constexpr int HAS_NOT_PLAYED_HUNT{0};
constexpr int PLAYTIME_INFO_PRIVATE{-1};
constexpr int GAMES_TAB_PRIVATE{-2};
constexpr int PROFILE_PRIVATE{-3};
constexpr int NO_HUNT_IN_LIBRARY{-4};
constexpr int UNKNOWN_ERROR{-5};
}  // namespace playTimeDetectionErrors

#endif
