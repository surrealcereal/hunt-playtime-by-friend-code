#include "user.hpp"

#include <map>
#include <string>
#include <vector>

#include "api.hpp"
#include "errorstatus.hpp"
#include "json.hpp"
#include "playtimeErrorCodes.hpp"

using json = nlohmann::json;
using string = std::string;

void User::setSteamID64() {
    constexpr long long steamid32to64ConversionFactor{76561197960265728};  // on linux this works fine with long but windows requires long long
    steamid64 = steamid32 + steamid32to64ConversionFactor;
}

void User::setUserDetails(const string& steamAPIKey, ErrorStatus& errorStatus) {
    auto usernameJSON{getSteamJSON("username", *this, steamAPIKey, errorStatus)};
    username = usernameJSON[0]["response"]["players"][0]["personaname"];
    constexpr int profilePrivateIndicator{3};
    profilePrivate = (usernameJSON[0]["response"]["players"][0][""] == profilePrivateIndicator);
}

void User::setHuntPlayTime(const string& steamAPIKey, ErrorStatus& errorStatus) {
    if (profilePrivate) {
        playtime = playtimeErrorCodes::PROFILE_PRIVATE;
        return;
    }
    auto playtimeJSON{getSteamJSON("playtime", *this, steamAPIKey, errorStatus)};
    constexpr int huntSteamGameID{594650};
    try {
        auto playtimes{playtimeJSON[0]["response"]["games"].get<std::vector<std::map<string, int>>>()};
        for (auto& game : playtimes) {
            if (playtimeInfoPrivate && game["playtime_forever"] > 0) {  // if playtime information is available at all
                playtimeInfoPrivate = false;
            }
            for (const auto& [key, value] : game) {
                if (key == "appid" && value == huntSteamGameID) {
                    playtime = game["playtime_forever"] / 60;
                    if (playtime == 0 && playtimeInfoPrivate) {
                        playtime = playtimeErrorCodes::PLAYTIME_INFO_PRIVATE;
                    }
                    return;  // if 0: playtime info private or hasn't played yet
                }
            }
        }
        playtime = playtimeErrorCodes::NO_HUNT_IN_LIBRARY;
        return;                                         // playtimes exist but none exist for Hunt
    } catch (const nlohmann::detail::type_error& ex) {  // cannot init playtimes correctly
        playtime = playtimeErrorCodes::GAMES_TAB_PRIVATE;
        return;
    }
    playtime = playtimeErrorCodes::UNKNOWN_ERROR;
}

User::User(int arg_steamid32, const std::string& steamAPIKey, ErrorStatus& errorStatus) {
    steamid32 = arg_steamid32;
    setSteamID64();
    setUserDetails(steamAPIKey, errorStatus);
    if (errorStatus) {
        return;
    }
    setHuntPlayTime(steamAPIKey, errorStatus);
}

string User::getUsername() { return username; }
int User::getSteamID32() { return steamid32; }
long long User::getSteamID64() { return steamid64; }
double User::getPlaytime() { return playtime; }
