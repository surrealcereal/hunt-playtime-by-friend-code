#ifndef API_HPP
#define API_HPP

#include <string>

#include "user.hpp"
#include "json.hpp"

std::string getWebpageContent(const char* url);
nlohmann::json getSteamJSON(const std::string& callType, User& user, const std::string& steamAPIKey, ErrorStatus& errorStatus);

#endif
